;; [[file:~/.spacemacs.d/spacemacs.org::*rss%20feeds][rss feeds:1]]
;; (with-eval-after-load 'elfeed
;;   (elfeed-search-set-filter "@2-days-ago"))
(setq rmh-elfeed-org-files (list "~/.spacemacs.d/elfeed.org"))
;; rss feeds:1 ends here

;; [[file:~/.spacemacs.d/spacemacs.org::*hugo%20blog%20export][hugo blog export:1]]
(use-package ox-hugo
  :after ox)
;; hugo blog export:1 ends here

;; [[file:~/.spacemacs.d/spacemacs.org::*mail][mail:1]]
;; mail
(setq mu4e-maildir "/home/saadnpq/.Mail"
      mu4e-sent-folder   "/Sent"
      mu4e-drafts-folder "/Drafts"
      mu4e-trash-folder  "/Trash"
      mu4e-enable-mode-line t
      mu4e-update-interval 300
      mu4e-get-mail-command "offlineimap"
      mu4e-enable-notifications t
      user-mail-address "saad@saadnpq.com"
      )

(setq message-send-mail-function 'message-send-mail-with-sendmail)
(setq sendmail-program "/usr/bin/msmtp")
(setq message-sendmail-extra-arguments '("--logfile=/home/saadnpq/.logs/msmtp"))

(with-eval-after-load 'mu4e-alert
      (mu4e-alert-set-default-style 'libnotify))
;; mail:1 ends here

;; [[file:~/.spacemacs.d/spacemacs.org::*font][font:1]]
(set-fontset-font "fontset-default" '(#x600 . #x6ff)
                  (font-spec :family "Kawkab Mono" :size 12 ))
;; font:1 ends here

;; [[file:~/.spacemacs.d/spacemacs.org::*bidi][bidi:1]]
(setq visual-order-cursor-movement t)
;; bidi:1 ends here

;; [[file:~/.spacemacs.d/spacemacs.org::*overriding%20the%20evil%20movement%20functions][overriding the evil movement functions:1]]
(evil-define-motion evil-forward-char (count &optional crosslines noerror)
  "this function overrides the evil's function due to issues in bidi text"
  :type exclusive
  (interactive "<c>" (list evil-cross-lines
                           (evil-kbd-macro-suppress-motion-error)))
  (evil-motion-loop (nil (or count 1))
    (right-char)
    ;; don't put the cursor on a newline
    (when (and evil-move-cursor-back
               (not evil-move-beyond-eol)
               (not (evil-visual-state-p))
               (not (evil-operator-state-p))
               (eolp) (not (eobp)) (not (bolp)))
      (right-char))))

(evil-define-motion evil-backward-char (count &optional crosslines noerror)
  "this function overrides the evil's function due to issues in bidi text"
  :type exclusive
  (interactive "<c>" (list evil-cross-lines
                           (evil-kbd-macro-suppress-motion-error)))
  (evil-motion-loop (nil (or count 1))
    (left-char)
    ;; don't put the cursor on a newline
    (unless (or (evil-visual-state-p) (evil-operator-state-p))
      (evil-adjust-cursor))))
;; overriding the evil movement functions:1 ends here

;; [[file:~/.spacemacs.d/spacemacs.org::*misc][misc:1]]
(setq scroll-margin 5)
(setq vc-follow-symlinks t)
(setq auto-save-default nil)
;; misc:1 ends here

;; [[file:~/.spacemacs.d/spacemacs.org::*delete%20buffer%20when%20deleting%20frame][delete buffer when deleting frame:1]]
(defun maybe-delete-frame-buffer (frame)
"When a dedicated FRAME is deleted, also kill its buffer.
A dedicated frame contains a single window whose buffer is not
displayed anywhere else."
  (let ((windows (window-list frame)))
    (when (eq 1 (length windows))
      (let ((buffer (window-buffer (car windows))))
        (when (eq 1 (length (get-buffer-window-list buffer nil t)))
          (kill-buffer buffer))))))

;; call this function whenever a frame is deleted
(add-to-list 'delete-frame-functions #'maybe-delete-frame-buffer)
;; delete buffer when deleting frame:1 ends here

;; [[file:~/.spacemacs.d/spacemacs.org::*Find%20this%20file][Find this file:1]]
(defun spacemacs/find-config-file ()
  (interactive)
  (find-file (concat dotspacemacs-directory "/spacemacs.org")))

(spacemacs/set-leader-keys "fec" 'spacemacs/find-config-file)
;; Find this file:1 ends here
