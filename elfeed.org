* Feeds  :elfeed:
** [[https://www.archlinux.org/feeds/news/][Arch news]]                                                     :linux:news:
** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCFzGyNKXPAglNq28qWYTDFA][kai hendry]]                                                        :videos:
** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCsJXkw_Ssp-1myJFm4_SMJA][Seorenn]]                                                            :emacs:
** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC2eYFnH61tmytImy1mTYvhA][uncle luke]]                                                  :linux:videos:
** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCVls1GmFKf6WlTraIb_IaJg][DistroTube ]]                                                 :linux:videos:
** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCkf4VIqu3Acnfzuk3kRIFwA][gotbletu ]]                                                   :linux:videos:
** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCVFkW-chclhuyYRbmmfwt6w][matrix]]                                                            :videos:
** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCFk8kgNu_bqsRZewxMGqkzQ][emacs sf ]]                                                          :emacs:
** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCA071Pllf2wk-B8Rkwt47bQ][anonumous]]                                                         :vidoes:
** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCRjSj54Xc9IlJwK1BgXjRMg][ahmed zayed ]]                                               :videos:arabic:
** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCk9NvmsPBC3lTn_L9kFaylA][ibsd ]]                                                       :linux:videos:
** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCEfFUaIkjbI06PhALdcXNVA][emacs cast  ]]                                                :emacs:videos:
** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC-lHJZR3Gqxm24_Vd_AJ5Yw][pewds ]]                                                            :videos:
** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCVO8YsD87ibqRGIJ7o8sOzg][price of zimbabwe]]                                                 :videos:
** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCFFLP0dKesrKWccYscdAr9A][the urban penguin]]                                           :linux:videos:
** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCZeO8ZK8fdgfK-srYXRa8mQ][wahbasss]]                                                   :videos:arabic:
** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCsnGwSIHyoYN0kiINAGUKxg][Wolfgang's Channel]]                                          :linux:videos:
** [[http://sachachua.com/blog/feed/][sachachua]]                                                          :emacs:
** [[https://destinationlinux.org/feed/mp3/][Destination Linux]]                                           :videos:linux:
** [[http://static.fsf.org/fsforg/rss/news.xml][FSF News]]                                                      :linux:news:
** [[https://www.linuxjournal.com/node/feed][Linux Journal]]                                                      :linux:
** [[https://github.com/saadnpq.private.atom?token=ArF7YbDl6LLRG93S8VmU6K0IiCj_Q4-mks66oOoNwA==][my github]]                                                         :github:
** [[http://vault.lunduke.com/LundukeShowMP3.xml][The Lunduke Show ]]                                           :linux:videos:
** [[https://spreadprivacy.com/rss/][DuckDuckGo Blog ]]                                                   :blogs:
** [[https://xkcd.com/rss.xml][xkcd.com            ]]                                              :comics:
** [[https://saadnpq.com/feed.xml][saadnpq]]                                                            :blogs:
** [[https://www.with-emacs.com/rss.xml][with emacs]]                                                         :blogs:

